package com.example.web;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;

public class ClassicBodyTagHandler extends BodyTagSupport {

	private static final long serialVersionUID = 6269087665013872992L;

	@Override
	public int doStartTag() throws JspException {
		return EVAL_BODY_BUFFERED; // default value
	}

	@Override
	public int doEndTag() throws JspException {
		JspWriter out = pageContext.getOut();
		try {
			out.println("in doEndTag()");
		} catch (IOException ex) {
			throw new JspException("IOException- " + ex.toString());
		}
		return EVAL_PAGE; // evaluate whole page / default value
	}

	@Override
	public int doAfterBody() throws JspException {
		BodyContent bodyContent = getBodyContent();
		try {
			pageContext.getOut().println(bodyContent.toString());
		} catch (IOException ex) {
			throw new JspException("IOException- " + ex.toString());
		}
		return SKIP_BODY; // default value
	}
}
