package com.example.web;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.DynamicAttributes;
import javax.servlet.jsp.tagext.JspTag;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class SimpleTagHandler extends SimpleTagSupport implements DynamicAttributes {

	private List<String> movies = Arrays.asList("Monsoon Wedding", "Saved!", "Fahrenheit 9/11");
	private Map<String, Object> tagAttrs = new HashMap<String, Object>();

	// setAttribute
	public void setMovies(List<String> movies) {
		this.movies = movies;
	}

	@Override
	public void setDynamicAttribute(String uri, String name, Object value) throws JspException {
		tagAttrs.put(name, value);
	}

	@Override
	public void doTag() throws JspException, IOException {
		{
			// simple tag
			getJspContext().getOut().print("Test");
		}

		{
			// simple tag with body
			getJspBody().invoke(null);
		}
		{
			// simple tag with body which use expression
			getJspContext().setAttribute("message", "Hello");
			getJspBody().invoke(null);
		}
		{
			// simple tag with body which use collection expression
			for (String movie : movies) {
				getJspContext().setAttribute("movie", movie);
				getJspBody().invoke(null);
			}
		}
		{
			// simple tag with body which use collection expression
			for (String movie : movies) {
				getJspContext().setAttribute("movie", movie);
				getJspBody().invoke(null);
			}
		}
		{
			// simple tag with body which use collection expression
			for (String movie : movies) {
				getJspContext().setAttribute("movie", movie);
				getJspBody().invoke(null);
			}
		}
		{
			// simple tag with dynamic attributes
			for (String attrName : tagAttrs.keySet()) {
				Object attrValue = tagAttrs.get(attrName);
				getJspContext().getOut().print((String) attrValue);
			}
		}
		{
			// simple tag with nested tag, find direct parent
			JspTag parent = getParent();
			getJspContext().getOut().println(parent.toString());
		}
		{
			// simple tag with nested tag, find first specific tag ancestor
			SimpleTagHandler ancestor = (SimpleTagHandler) findAncestorWithClass(this, SimpleTagHandler.class);
			getJspContext().getOut().println(ancestor.toString());
		}
	}

}
