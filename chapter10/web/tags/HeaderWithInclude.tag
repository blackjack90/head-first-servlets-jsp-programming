<!-- http://docs.oracle.com/javaee/1.4/tutorial/doc/JSPTags5.html -->
<!-- https://today.java.net/pub/a/today/2003/11/14/tagfiles.html?page=1 -->

This tag file shows the use of the include directive. 
The first include directive demonstrates how you can include
a static resource called included.html.
<br/>
Here is the content of included.html:
<%@ include file="include.html" %>
<br/>
<br/>
The second include directive includes another 
dynamic resource: HeaderWithAttribute.tag.
<br/>
<%@ include file="HeaderWithAttribute.tag" %>