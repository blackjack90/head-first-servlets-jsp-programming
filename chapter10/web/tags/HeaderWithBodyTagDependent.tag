<!-- http://docs.oracle.com/javaee/1.4/tutorial/doc/JSPTags5.html -->
<%@ attribute name="fontColor" required="true"%>
<!-- scriptless and not contains EL  -->
<%@ tag body-content="tagdependent"%>

Header
<br />
<em><strong><font color=”${fontColor}”><jsp:doBody/></font></strong></em>