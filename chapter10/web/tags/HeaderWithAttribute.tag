<!-- http://docs.oracle.com/javaee/1.4/tutorial/doc/JSPTags5.html -->
<%@ tag import="java.util.Date" import="java.text.DateFormat"%>
<%@ attribute name="subTitle" required="true" rtexprvalue="true"%>

Header<br/>
<em><strong>${subTitle}</strong></em><br/>
<% 
  DateFormat dateFormat = 
    DateFormat.getDateInstance(DateFormat.LONG);
  Date now = new Date(System.currentTimeMillis());
  out.println(dateFormat.format(now));
%>