<%@ taglib prefi="myTags" uri="classicTags"%>
<html>
<body>
	<!-- classic tag: -->
	<myTags:classicTag />

	<!-- classic tag with body: -->
	<myTags:classicTagWithBody>
      This is the body
    </myTags:classicTagWithBody>

	<!--  classic tag with expression body: -->
	<myTags:classicTagWithExpressionBody>
      This is the body with ${message}
    </myTags:classicTagWithExpressionBody>

	<!-- classic tag with collection expression body: -->
	<table border="1">
		<myTags:classicTagWithCollectionExpressionBody>
			<tr>
				<td>${movie}</td>
			</tr>
		</myTags:classicTagWithCollectionExpressionBody>
	</table>

	<!-- classic tag with attribute: -->
	<table>
		<myTags:classicTagWithAttribute movieList="${movieCollection}">
			<tr>
				<td>${movie.name}</td>
				<td>${movie.genre}</td>
			</tr>
		</myTags:classicTagWithAttribute>
	</table>

	<!--  classic tag with parent tag: -->
	<myTags:classicTagWithBody>
		<myTags:classicTag />
	</myTags:classicTagWithBody>
</body>
</html>