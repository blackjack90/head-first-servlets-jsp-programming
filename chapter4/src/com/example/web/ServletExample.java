package com.example.web;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ServletExample extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		// Comes from ServletConfig interface
		// one instance per servlet
		// passing deploy time information to servlet in DD
		getInitParameter("parameterName");
		getInitParameterNames();
		ServletContext servletContext = getServletContext();
		getServletName();
		
		// Comes from ServletContext interface
		// one instance per one JVM
		// access web app parameters
		servletContext.getMajorVersion();
		servletContext.getMinorVersion();
		servletContext.getAttribute("attributeName");
		servletContext.getMimeType("file");
		servletContext.getServerInfo();
		servletContext.log("msg");
		
		//-------------------------------------------------------------------------------------------------------------------
		
		// servlet request
		request.getAttribute("name");
		request.getContentLength();
		request.getInputStream();
		request.getRemotePort();
		request.getServerPort();
		request.getLocalPort();
		request.getParameter("name");
		request.getParameterNames();
		
		// http servlet request
		request.getHeader("name");
		request.getIntHeader("name");
		request.getHeaderNames();
		request.getQueryString();
		request.getCookies();
		request.getSession();
		request.getMethod();
		
		//-------------------------------------------------------------------------------------------------------------------
		
		// servlet response
		response.getBufferSize();
		response.getContentType();
		response.setContentLength(0);
		response.getOutputStream();
		response.getWriter();
		
		// http servlet response
		response.addCookie(new Cookie("name", "value"));
		response.addHeader("name", "value");
		response.setHeader("name", "value");
		response.setIntHeader("name", 0);
		response.encodeRedirectURL("url");
		response.sendError(HttpServletResponse.SC_MOVED_TEMPORARILY);
		response.setStatus(HttpServletResponse.SC_OK);
		
		//-------------------------------------------------------------------------------------------------------------------
		
		// client-side redirection
		// use relative path
		// If the response has already been committed, this method throws an IllegalStateException
		response.sendRedirect("www.euedge.com");
		
		// server-side redirection
		// absolute and relative path is available
		RequestDispatcher view = request.getRequestDispatcher("result.jsp");
		view.forward(request, response);
		view.include(request, response);
		
		// just absolute path is available
		RequestDispatcher otherView = getServletContext().getRequestDispatcher("/result.jsp");
		otherView.forward(request, response);
		otherView.include(request, response);
	}
}