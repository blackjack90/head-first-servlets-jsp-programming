<%@ taglib pref x="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page errorPage="errorPage.jsp"%>
<html>
<body>
	About to do a risky thing:
	<br>
	<c:catch var="myException">
    Inside the catch...
    <%
		int x = 10 / 0;
	%>
	</c:catch>
	<c:if test="${myException != null}">
    There was an exception: ${myException.message}  <br>
	</c:if>
	We survived.
</body>
</html>