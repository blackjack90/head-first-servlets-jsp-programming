package com.example.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class HttpSessionThrowsIllegalExceptionServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	// throws IllegalException because use getAttribute() after invalidate()

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();
		session.setAttribute("foo", "42");
		session.setAttribute("bar", "420");
		session.invalidate();
		String foo = (String) session.getAttribute("foo");
		out.println("�Foo: " + foo);
	}

	// throws IllegalException because use getAttribute() after invalidate with
	// setMaxInactiveInterval(0)

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();
		session.setAttribute("foo", "42");
		session.setMaxInactiveInterval(0);
		String foo = (String) session.getAttribute("foo");
		if (session.isNew()) {
			out.println("This is a new session.");
		} else {
			out.println("Welcome back!");
		}
		out.println("Foo: " + foo);
	}

}
