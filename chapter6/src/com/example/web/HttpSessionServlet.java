package com.example.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class HttpSessionServlet extends HttpServlet {

	private static final long serialVersionUID = -6209501983534514231L;

	// demonstrate HttpSession interface
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("test session<br>");

		HttpSession session = request.getSession();

		session.getId();
		session.getAttributeNames();
		session.getAttribute("name");
		session.setAttribute("name", "value");
		session.getCreationTime();
		session.getLastAccessedTime();
		session.setMaxInactiveInterval(600); // set timeout in seconds
		session.getMaxInactiveInterval();
		session.invalidate();
		session.isNew();
	}

	
}
