package com.example.web;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;

public class MyServletRequestListener implements ServletRequestListener {

	// You want to know each time a	request comes in, so that you can log it
	// life-cycle listener, need to register in DD
	
	@Override
	public void requestDestroyed(ServletRequestEvent sre) {
		System.out.println("Destroyed request local name: " + sre.getServletRequest().getLocalName());
	}

	@Override
	public void requestInitialized(ServletRequestEvent sre) {
		System.out.println("Initialized request local name: " + sre.getServletRequest().getLocalName());
	}

}
