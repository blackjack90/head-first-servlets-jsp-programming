package com.example.web;

import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;

public class MyServletContextAttributeListener implements ServletContextAttributeListener {

	// You want to know if an attribute in a web app context has been added, removed, or replaced
	// attribute listener, need to register in DD

	@Override
	public void attributeAdded(ServletContextAttributeEvent event) {
		System.out.println("Added attribute name: " + event.getName() + " attribute value: " + event.getValue());
	}

	@Override
	public void attributeRemoved(ServletContextAttributeEvent event) {
		System.out.println("Removed attribute name: " + event.getName() + " attribute value: " + event.getValue());
	}

	@Override
	public void attributeReplaced(ServletContextAttributeEvent event) {
		System.out.println("Replaced attribute name: " + event.getName() + " attribute value: " + event.getValue());
	}

}
