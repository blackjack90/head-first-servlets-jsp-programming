package com.example.web;

import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

public class MyHttpSessionAttributeListener implements HttpSessionAttributeListener{

	// You want to know when a session attribute has been added, removed, or replaced
	// attribute listener, need to register in DD
	
	@Override
	public void attributeAdded(HttpSessionBindingEvent event) {
		System.out.println("Added attribute name: " + event.getName() + " attribute value: " + event.getValue());
	}
	
	@Override
	public void attributeRemoved(HttpSessionBindingEvent event) {
		System.out.println("Removed attribute name: " + event.getName() + " attribute value: " + event.getValue());
	}

	@Override
	public void attributeReplaced(HttpSessionBindingEvent event) {
		System.out.println("Replaced attribute name: " + event.getName() + " attribute value: " + event.getValue());
	}

}
