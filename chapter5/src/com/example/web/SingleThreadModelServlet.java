package com.example.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.SingleThreadModel;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@SuppressWarnings("deprecation")
public class SingleThreadModelServlet extends HttpServlet implements SingleThreadModel {

	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("test context attributes<br>");
		HttpSession session = request.getSession();

		session.setAttribute("foo", "22");
		session.setAttribute("bar", "42");
		out.println(session.getAttribute("foo"));
		out.println(session.getAttribute("bar"));
	}

}
