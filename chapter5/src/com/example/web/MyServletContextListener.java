package com.example.web;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class MyServletContextListener implements ServletContextListener {

	// You want to know if a context has been created or destroyed
	// life-cycle listener, need to register in DD

	public void contextInitialized(ServletContextEvent event) {
		// code to initialize the database connection or any other object
		// and store it as a context attribute
		ServletContext sc = event.getServletContext();
		String dogBreed = sc.getInitParameter("breed");
		Dog d = new Dog(dogBreed);
		sc.setAttribute("dog", d);
	}

	public void contextDestroyed(ServletContextEvent event) {
		// code to close the database connection or dereference it
		ServletContext sc = event.getServletContext();
		sc.removeAttribute("dog");
	}

}
