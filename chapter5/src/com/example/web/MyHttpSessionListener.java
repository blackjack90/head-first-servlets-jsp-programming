package com.example.web;

import java.util.Date;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class MyHttpSessionListener implements HttpSessionListener {

	// You want to track the active	sessions
	// life-cycle listener, need to register in DD
	
	@Override
	public void sessionCreated(HttpSessionEvent se) {
		System.out.println("Created session id: " +  se.getSession().getId() + " at: " + new Date());
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		System.out.println("Destroyed session id: " +  se.getSession().getId() + " at: " + new Date());
	}

}
